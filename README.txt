SMS Vianett Module
===============================

This module enables the SMS Framework to send SMS via the Vianett Gateway
(vianett.com)

# Depencencies

- smsframework
- vianett-php library
- php-curl extension (used by library)

# Installation
- Download vianett-php library from https://github.com/zaporylie/vianett-php
 to sites/all/libraries/vianett folder.
- Enable sms_vianett module.
- Configure vianett gateway (admin/smsframework/gateways/vianett)
- Enable vianett gateway (admin/smsframework/gateways)

# Credits
- Front Kommunikasjon
- Ny Media AS
